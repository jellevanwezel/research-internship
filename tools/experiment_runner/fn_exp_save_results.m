function [ ] = fn_exp_save_results( results, resultsPath )

for i = 1 : size(results, 1)
   
    result = results{i};
    resultPath = [resultsPath, '/', result.dataSetName, '/', result.function(8:end), '/'];
    [~, ~, ~] = mkdir(resultPath);
    
    paramString = '';
    for paramIdx = 1:size(result.params, 2)
        paramString = strcat(paramString, '_', num2str(result.params{paramIdx}));
    end
    if size(paramString, 2) > 1
        paramString = ['(',paramString(2:end),')'];
    end
    
    fileName = ['results_', result.function(8:end),'_', result.date, '_', paramString,'.mat'];
    save([resultPath, '/', fileName], 'result');
    
end

end

