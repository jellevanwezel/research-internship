function [ experiments ] = fn_exp_generator(funName, datasets, dataPath, preproc, runs, params)

paramCombs = combvec(params{:});
paramSize = size(paramCombs,2);
if paramSize == 0
    paramSize = 1;
end
expSize = paramSize * size(preproc,2) * size(datasets,2);
experiments = cell(1,expSize);
expCount = 0;

for datasetIdx = 1:size(datasets,2)
    dataSetName = datasets{datasetIdx};
    dataSetPath = [dataPath,'/',dataSetName,'/',dataSetName,'.mat'];
    load(dataSetPath,'data','labels');
    
    for preProsIdx = 1 : size(preproc,2)
        preProsFnName = ['fn_pre_',preproc{preProsIdx}];
        preProsFn = str2func(preProsFnName);
        preProsData = preProsFn(data);


        expCount = expCount + 1;
        experiment = struct();
        experiment.dataSetName = dataSetName;
        experiment.function = ['fn_exp_',funName];
        experiment.preprocessing = preProsFnName;
        experiment.runs = runs;
        experiment.dataset = struct('data',preProsData,'labels',labels);
        experiment.dataSplits = cell(runs,1);

        for run = 1:runs
            experiment.dataSplits{run} = fn_pre_splitter(preProsData, labels);
        end

        experiment.params = {};
        experiments{1,expCount} = experiment;

        for paramCombsIdx = 1:size(paramCombs,2)
            paramComb = paramCombs(:,paramCombsIdx)';
            experiment.params = num2cell(paramComb);
            experiments{1,expCount} = experiment; %overwrite the first one
            if paramCombsIdx ~= size(paramCombs,2) %dont count the last one
                expCount = expCount + 1;
            end
        end
    end
end
end