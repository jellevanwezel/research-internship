function [ dataset ] = fn_pre_splitter( data, labels )
    splitIndices = fn_pre_nFoldCrossValidation(data, 'nb_folds', 2, 'labels', labels);
    trainData = data(splitIndices{1},:);
    trainLabels = labels(splitIndices{1},:);
    testData = data(splitIndices{2},:);
    testLabels = labels(splitIndices{2},:);
    dataset = struct('trainData',trainData, 'trainLabels', trainLabels, 'testData', testData, 'testLabels', testLabels);
end

