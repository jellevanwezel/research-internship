funName = 'suvrel';
dataSets = {'balance-scale','breast-cancer','iris','parkinsons','pima-indians-diabetes','seeds','segmentation','wine'};
dataPath = '/home/jelle/RUG/RI/data/';
resultsPath = '/home/jelle/RUG/RI/results/';
preproc = {'naive','normalized','normalized_var'};
%        {runs}
params = {10};

expermiments = fn_exp_generator(funName,dataSets,dataPath,preproc,params);
results = fn_exp_runner(expermiments);
fn_exp_save_results(results, resultsPath);

