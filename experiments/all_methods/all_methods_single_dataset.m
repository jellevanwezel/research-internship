clear;
dataSets = {'wine'};
preproc = {'naive','normalized','normalized_var'};
dataPath = '/home/jelle/RUG/RI/data/';
resultsPath = '/home/jelle/RUG/RI/results/';
impPath = '/home/jelle/RUG/RI/implementation/';
addpath(genpath(impPath));

funName = 'gml';
params = {500,[10^-2,10^-3,10^-4]};
runs = 10;
expermimentsGml = fn_exp_generator(funName,dataSets,dataPath,preproc,runs,params);

funName = 'suvrel';
params = {};
runs = 10;
expermimentsSuvrel = fn_exp_generator(funName,dataSets,dataPath,preproc,runs,params);

funName = 'suvrell';
params = {0:0.1:10};
runs = 10;
expermimentsSuvrell = fn_exp_generator(funName,dataSets,dataPath,preproc,runs,params);

funName = 'suvrel_to_gml';
params = {500,[10^-2,10^-3,10^-4]};
runs = 10;
expermimentsSuvrelGml = fn_exp_generator(funName,dataSets,dataPath,preproc,runs,params);

experiments = [expermimentsGml, expermimentsSuvrel, expermimentsSuvrell, expermimentsSuvrelGml];

results = fn_exp_runner(experiments);
fn_exp_save_results(results, resultsPath);