%         'gml', 'suvrel','suvrell', 'suvrel_to_gml'
funName = 'suvrell';

% datasets:  {    
%                   'balance-scale',
%                   'breast-cancer',
%                   'iris',
%                   'parkinsons',
%                   'pima-indians-diabetes',
%                   'seeds',
%                   'segmentation',
%                   'wine'
%             }
dataSets = {'wine'};
dataPath = '/home/jelle/RUG/RI/data/';
resultsPath = '/home/jelle/RUG/RI/results/';

%          'naive','normalized','normalized_var'
preproc = {'naive','normalized','normalized_var'};
%preproc = {'naive'};

%        {runs,gamma}
%params = {10};
params = {10,0:0.1:15};

expermiments = fn_exp_generator(funName,dataSets,dataPath,preproc,params);
results = fn_exp_runner(expermiments);
fn_exp_save_results(results, resultsPath);