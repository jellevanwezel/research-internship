clear;
funName = 'suvrel';
dataSets = {'wine'};
dataPath = '/home/jelle/RUG/RI/data/';
resultsPath = '/home/jelle/RUG/RI/results/';
impPath = '/home/jelle/RUG/RI/implementation/';
addpath(genpath(impPath));


preproc = {'naive','normalized','normalized_var'};
%        {runs}
params = {};
runs = 10;

expermiments = fn_exp_generator(funName,dataSets,dataPath,preproc,runs, params);
results = fn_exp_runner(expermiments);
fn_exp_save_results(results, resultsPath);

